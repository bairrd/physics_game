package achievements;

import org.newdawn.slick.SavedState;

import entity.Player;

import states.GameplayState;

public class AchWinner extends Achievement {
	private static String savedDone = "achWinnerDone";
	
	public AchWinner(){
		super();
	}

	@Override
	public void observe(GameplayState gameState) {
		if(done)
			return;
		
		Player thePlayer = gameState.getWorld().getPlayer();
		
		if(thePlayer.winner == true)
			this.done = true;
	}

	@Override
	public void save(SavedState savedGameData) {
		savedGameData.setString(savedDone, Boolean.toString(done));
	}

	@Override
	public void load(SavedState savedGameData) {
		try{
			this.done = Boolean.parseBoolean(savedGameData.getString(savedDone, "false"));
		} catch(IllegalArgumentException e){
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString(){
		String doneString = "nope.";
		if(done)
			doneString = "you are.";
		return "Winning: " + doneString + "\n";
	}
}
