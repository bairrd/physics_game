package achievements;

import org.newdawn.slick.SavedState;

import states.GameplayState;

public interface AchievementInterface {
	public boolean isDone();

	public void observe(GameplayState gameState);
	/* override this with whatever the cheevo is!
	 * don't forget to skip this if done
	 */

	public void save(SavedState savedGameData);
	/* save the achievement status into savedGameData
	 * use strings and numbers, declare clearly!
	 */

	public void load(SavedState savedGameData);
	/* load the achievement status from savedGameData
	 * if any field is not found, just use defaults
	 */
}
